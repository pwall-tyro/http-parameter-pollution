package app;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.ResponseEntity;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Value;
import app.exception.BadRequestException;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@RestController
public class VerifyController {

    @Value("${paymenturl}")
    private String paymentUrl;

    @PostMapping("/")
    public String res(HttpServletRequest request) {
        try {
            String action = request.getParameter("action");
            String amount = request.getParameter("amount");
            if (action.equals("transfer")) {
                Amount amt = new Amount(amount);
                System.out.println("Verify Controller: Going to transfer $"+amt);
                RestTemplate restTemplate = new RestTemplate();
                String fakePaymentUrl = this.paymentUrl;  //Internal fake payment micro-service
                Map<String, String> params = new HashMap<>();
                params.put("action", action);
                params.put("amount", amt.toString());
                ResponseEntity<String> response
                    = restTemplate.getForEntity(
                            fakePaymentUrl,
                            String.class,
                        params);
                return response.getBody();
            } else if (action.equals("withdraw")) {
                return "Verify Controller: Sorry, you can only make transfer";
            } else {
                return "Verify Controller: You must specify action: transfer or withdraw";
            }
        } catch(RuntimeException ex) {
            throw new BadRequestException();
        }
    }

}
