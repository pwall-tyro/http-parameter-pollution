package app;

import java.math.BigInteger;

public class Amount {

    private BigInteger value;

    public Amount(BigInteger value) {
        this.value = value;
    }

    public Amount(String str) {
        value = new BigInteger(str);
    }

    public Amount(int i) {
        value = BigInteger.valueOf(i);
    }

    @Override
    public String toString() {
        return value.toString();
    }

}
