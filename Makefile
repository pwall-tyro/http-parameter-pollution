APP=insecure-programming-app

all: build

test:
	docker-compose -f docker-compose.test.yml up -d payment
	docker-compose -f docker-compose.test.yml build --build-arg GRADLETASK=test verify

securitytest:
	docker-compose -f docker-compose.test.yml up -d payment
	docker-compose -f docker-compose.test.yml build --build-arg GRADLETASK=securitytest verify

build:
	docker-compose build

run:
	docker-compose -f docker-compose.test.yml up

clean:
	docker-compose down
	docker-compose rm -f
	docker-compose -f docker-compose.test.yml down
	docker-compose -f docker-compose.test.yml rm -f
	docker system prune

.PHONY: all test securitytest clean
